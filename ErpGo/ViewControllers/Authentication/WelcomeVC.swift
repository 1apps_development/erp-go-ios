//
//  WelcomeVC.swift
//  ErpGo
//
//  Created by DREAMWORLD on 31/03/22.
//

import UIKit

class WelcomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginTypeVC") as! LoginTypeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnTap_Register(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterTypeVC") as! RegisterTypeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
