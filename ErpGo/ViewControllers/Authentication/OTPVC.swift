//
//  OTPVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 21/03/22.
//

import UIKit

class OTPVC: UIViewController {
    @IBOutlet weak var btn_sendcode: UIButton!
    @IBOutlet weak var View_email: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setBorder(viewName: self.View_email, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_email.frame.height / 2)
        cornerRadius(viewName: self.btn_sendcode, radius: self.btn_sendcode.frame.height / 2)
    }
    @IBAction func btnTap_SendCode(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordSucessVC") as! ChangePasswordSucessVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
