//
//  ChangePasswordSucessVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 21/03/22.
//

import UIKit

class ChangePasswordSucessVC: UIViewController {

    @IBOutlet weak var btn_home: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        cornerRadius(viewName: self.btn_home, radius: self.btn_home.frame.height / 2)
    }
    
    @IBAction func btnTap_GotoHome(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
