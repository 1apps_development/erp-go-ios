//
//  RegisterVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 21/03/22.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var btn_Register: UIButton!
    @IBOutlet weak var View_Password: UIView!
    @IBOutlet weak var View_Email: UIView!
    @IBOutlet weak var View_FullName: UIView!
    @IBOutlet weak var View_Phone: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBorder(viewName: self.View_FullName, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_FullName.frame.height / 2)
        setBorder(viewName: self.View_Phone, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_Phone.frame.height / 2)
        setBorder(viewName: self.View_Email, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_Email.frame.height / 2)
        setBorder(viewName: self.View_Password, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_Password.frame.height / 2)
        
        cornerRadius(viewName: self.btn_Register, radius: self.btn_Register.frame.height / 2)
        
    }
    
    @IBAction func btnTap_signup(_ sender: UIButton) {
        
    }
    
    @IBAction func btnTap_Signin(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginTypeVC") as! LoginTypeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
