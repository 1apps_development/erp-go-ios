//
//  ForgotpasswordVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 21/03/22.
//

import UIKit

class ForgotpasswordVC: UIViewController {

    @IBOutlet weak var btn_sendcode: UIButton!
    @IBOutlet weak var View_email: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        setBorder(viewName: self.View_email, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_email.frame.height / 2)
        cornerRadius(viewName: self.btn_sendcode, radius: self.btn_sendcode.frame.height / 2)
    }
    @IBAction func btnTap_Sendcode(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
