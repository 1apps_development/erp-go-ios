//
//  MeetingListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class MeetingTableviewCell: UITableViewCell {
    
}

class MeetingListVC: UIViewController {

    @IBOutlet weak var tableviewNextMeeting_height: NSLayoutConstraint!
    @IBOutlet weak var tableviewMeetinglist_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_meetinglist: UITableView!
    @IBOutlet weak var tableview_nextmeeting: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewNextMeeting_height.constant = 105.0 * 1
        self.tableviewMeetinglist_height.constant = 105.0 * 10
    }

    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension MeetingListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableview_nextmeeting {
            return 1
        }
        else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableview_nextmeeting {
            let cell = self.tableview_nextmeeting.dequeueReusableCell(withIdentifier: "MeetingTableviewCell") as! MeetingTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_meetinglist.dequeueReusableCell(withIdentifier: "MeetingTableviewCell") as! MeetingTableviewCell
            return cell
        }
    }
}
