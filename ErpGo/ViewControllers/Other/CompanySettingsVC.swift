//
//  CompanySettingsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class CompanySettingsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
