//
//  ManageUsersVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class ManageUsersTableviewCell: UITableViewCell {
    
}

class ManageUsersVC: UIViewController {

    @IBOutlet weak var tableview_manageusers: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension ManageUsersVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_manageusers.dequeueReusableCell(withIdentifier: "ManageUsersTableviewCell") as! ManageUsersTableviewCell
        return cell
    }
}


