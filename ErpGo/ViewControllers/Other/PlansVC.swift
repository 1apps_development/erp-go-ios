//
//  PlansVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 12/04/22.
//

import UIKit

class PlanTableviewCell: UITableViewCell {
    
}

class PlansVC: UIViewController {

    @IBOutlet weak var tableview_plans: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_plans.estimatedRowHeight = 285.0
        self.tableview_plans.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension PlansVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_plans.dequeueReusableCell(withIdentifier: "PlanTableviewCell") as! PlanTableviewCell
        return cell
    }
}
