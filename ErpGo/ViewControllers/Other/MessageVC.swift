//
//  MessageVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 11/04/22.
//

import UIKit

class SendMessageTableviewCell: UITableViewCell {
    
}

class ReceiveMessageTableviewCell: UITableViewCell {
    
}

class MessageVC: UIViewController {

    @IBOutlet weak var tableview_message: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_message.estimatedRowHeight = 75.0
        self.tableview_message.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension MessageVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            let cell = self.tableview_message.dequeueReusableCell(withIdentifier: "SendMessageTableviewCell") as! SendMessageTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_message.dequeueReusableCell(withIdentifier: "ReceiveMessageTableviewCell") as! ReceiveMessageTableviewCell
            return cell
        }
    }
}
