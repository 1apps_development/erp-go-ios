//
//  NotificationsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class NotificationTableviewCell : UITableViewCell {
    
    @IBOutlet weak var view_container: UIView!
}

class NotificationsVC: UIViewController {

    @IBOutlet weak var tableview_notification: UITableView!
    @IBOutlet weak var btnWeek: UIButton!
    @IBOutlet weak var btnToday: UIButton!
    @IBOutlet weak var btnMonth: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnToday.layer.masksToBounds = true
        self.btnToday.layer.borderColor = UIColor.lightGray.cgColor
        self.btnToday.layer.borderWidth = 1.0
        self.btnToday.layer.cornerRadius = 15.0
        
        self.btnWeek.layer.masksToBounds = true
        self.btnWeek.layer.borderColor = UIColor.lightGray.cgColor
        self.btnWeek.layer.borderWidth = 1.0
        self.btnWeek.layer.cornerRadius = 15.0
        
        self.btnMonth.layer.masksToBounds = true
        self.btnMonth.layer.borderColor = UIColor.lightGray.cgColor
        self.btnMonth.layer.borderWidth = 1.0
        self.btnMonth.layer.cornerRadius = 15.0
        
        self.tableview_notification.estimatedRowHeight = 85.0
        self.tableview_notification.rowHeight = UITableView.automaticDimension
    }
    

}

extension NotificationsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_notification.dequeueReusableCell(withIdentifier: "NotificationTableviewCell") as! NotificationTableviewCell
        cell.view_container.layer.borderColor = UIColor.lightGray.cgColor
        cell.view_container.layer.borderWidth = 1.0
        cell.view_container.layer.masksToBounds = true
        cell.view_container.layer.cornerRadius = 10.0
        return cell
    }
    
    
}
