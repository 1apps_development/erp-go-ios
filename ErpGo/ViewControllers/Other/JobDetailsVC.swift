//
//  JobDetailsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 12/04/22.
//

import UIKit

class JobDetailsVC: UIViewController {

    @IBOutlet weak var tableviewJobs_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_jobs: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewJobs_height.constant = 185.0 * 3
    }

}

extension JobDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_jobs.dequeueReusableCell(withIdentifier: "JobTableviewCell") as! JobTableviewCell
        return cell
    }
}
