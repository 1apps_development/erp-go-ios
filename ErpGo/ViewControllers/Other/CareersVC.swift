//
//  CareersVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 12/04/22.
//

import UIKit

class JobTableviewCell: UITableViewCell {
    
}

class CareersVC: UIViewController {

    @IBOutlet weak var tableview_jobs: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_jobs.estimatedRowHeight = 185.0
        self.tableview_jobs.rowHeight = UITableView.automaticDimension
    }

}

extension CareersVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_jobs.dequeueReusableCell(withIdentifier: "JobTableviewCell") as! JobTableviewCell
        return cell
    }
}
