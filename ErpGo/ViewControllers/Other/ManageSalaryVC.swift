//
//  ManageSalaryVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class ManageSalaryTitleTableviewCell: UITableViewCell {
    
}

class ManageSalaryDataTableviewCell: UITableViewCell {
    
}

class ManageSalaryVC: UIViewController {

    @IBOutlet weak var tableviewSalary_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_manageSalary: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewSalary_height.constant = 60.0 * 11
    }

}

extension ManageSalaryVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_manageSalary.dequeueReusableCell(withIdentifier: "ManageSalaryTitleTableviewCell") as! ManageSalaryTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_manageSalary.dequeueReusableCell(withIdentifier: "ManageSalaryDataTableviewCell") as! ManageSalaryDataTableviewCell
            return cell
        }
    }
}
