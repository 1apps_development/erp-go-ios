//
//  CreateInvoiceVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 11/04/22.
//

import UIKit

class InvoiceItemTableviewCell: UITableViewCell {
    
}

class CreateInvoiceVC: UIViewController {

    @IBOutlet weak var tableviewInvoiceItem_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_invoiceItem: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewInvoiceItem_height.constant = 316.0 * 2
    }

}

extension CreateInvoiceVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 316.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_invoiceItem.dequeueReusableCell(withIdentifier: "InvoiceItemTableviewCell") as! InvoiceItemTableviewCell
        return cell
    }
}
