//
//  FormBuilderVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class FormBuilderTitleTableviewCell: UITableViewCell {
    
}

class FormBuilderDataTableviewCell: UITableViewCell {
    
}

class FormBuilderVC: UIViewController {

    @IBOutlet weak var tableviewFormBuilder_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_formbuilder: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewFormBuilder_height.constant = 50.0 * 11
    }

}

extension FormBuilderVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_formbuilder.dequeueReusableCell(withIdentifier: "FormBuilderTitleTableviewCell") as! FormBuilderTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_formbuilder.dequeueReusableCell(withIdentifier: "FormBuilderDataTableviewCell") as! FormBuilderDataTableviewCell
            return cell
        }
    }
}
