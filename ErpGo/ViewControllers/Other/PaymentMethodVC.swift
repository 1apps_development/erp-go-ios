//
//  PaymentMethodVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class PaymentMethodEnableCell : UITableViewCell {
    
    @IBOutlet weak var view_credentials: UIView!
}

class PaymentMethodDisableCell : UITableViewCell {
    
    @IBOutlet weak var view_credentials: UIView!
}

class PaymentMethodVC: UIViewController {

    @IBOutlet weak var tableview_paymentmethod: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PaymentMethodVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 316.0
        }
        else {
            return 120.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_paymentmethod.dequeueReusableCell(withIdentifier: "PaymentMethodEnableCell") as! PaymentMethodEnableCell
            return cell
        }
        else {
            let cell = self.tableview_paymentmethod.dequeueReusableCell(withIdentifier: "PaymentMethodDisableCell") as! PaymentMethodDisableCell
            return cell
        }
        
    }
    
    
}
