//
//  EmployeeSetSalaryVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 11/04/22.
//

import UIKit

class AllowanceTitleTableviewCell: UITableViewCell {
    
}

class AllowanceDataTableviewCell: UITableViewCell {
    
}

class CommisionTitleTableviewCell: UITableViewCell {
    
}

class CommisionDataTableviewCell: UITableViewCell {
    
}

class LoanTitleTableviewCell: UITableViewCell {
    
}

class LoanDataTableviewCell: UITableViewCell {
    
}

class SaturationTitleTableviewCell: UITableViewCell {
    
}

class SaturationDataTableviewCell: UITableViewCell {
    
}

class OtherPaymentTitleTableviewCell: UITableViewCell {
    
}

class OtherPaymentDataTableviewCell: UITableViewCell {
    
}

class OvertimeTitleTableviewCell: UITableViewCell {
    
}

class OvertimeDataTableviewCell: UITableViewCell {
    
}

class EmployeeSetSalaryVC: UIViewController {

    @IBOutlet weak var tableview_Allowance: UITableView!
    @IBOutlet weak var tableviewAllowance_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_commisions: UITableView!
    @IBOutlet weak var tableviewCommisions_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_loan: UITableView!
    @IBOutlet weak var tableviewLoan_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_saturation: UITableView!
    @IBOutlet weak var tableviewSaturation_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_otherpayment: UITableView!
    @IBOutlet weak var tableviewOtherPayment_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_overtime: UITableView!
    @IBOutlet weak var tableviewOvertime_height: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewAllowance_height.constant = 60.0 * 5
        self.tableviewCommisions_height.constant = 60.0 * 3
        self.tableviewLoan_height.constant = 60.0 * 2
        self.tableviewSaturation_height.constant = 60.0 * 4
        self.tableviewOtherPayment_height.constant = 60.0 * 2
        self.tableviewOvertime_height.constant = 60.0 * 5
    }

}

extension EmployeeSetSalaryVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableview_Allowance {
            return 5
        }
        else if tableView == self.tableview_commisions {
            return 3
        }
        else if tableView == self.tableview_loan {
            return 2
        }
        else if tableView == self.tableview_saturation {
            return 4
        }
        else if tableView == self.tableview_otherpayment {
            return 2
        }
        else if tableView == self.tableview_overtime {
            return 5
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableview_Allowance {
            if indexPath.row == 0 {
                let cell = self.tableview_Allowance.dequeueReusableCell(withIdentifier: "AllowanceTitleTableviewCell") as! AllowanceTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_Allowance.dequeueReusableCell(withIdentifier: "AllowanceDataTableviewCell") as! AllowanceDataTableviewCell
                return cell
            }
        }
        else if tableView == self.tableview_commisions {
            if indexPath.row == 0 {
                let cell = self.tableview_commisions.dequeueReusableCell(withIdentifier: "CommisionTitleTableviewCell") as! CommisionTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_commisions.dequeueReusableCell(withIdentifier: "CommisionDataTableviewCell") as! CommisionDataTableviewCell
                return cell
            }
        }
        else if tableView == self.tableview_loan {
            if indexPath.row == 0 {
                let cell = self.tableview_loan.dequeueReusableCell(withIdentifier: "LoanTitleTableviewCell") as! LoanTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_loan.dequeueReusableCell(withIdentifier: "LoanDataTableviewCell") as! LoanDataTableviewCell
                return cell
            }
        }
        else if tableView == self.tableview_saturation {
            if indexPath.row == 0 {
                let cell = self.tableview_saturation.dequeueReusableCell(withIdentifier: "SaturationTitleTableviewCell") as! SaturationTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_saturation.dequeueReusableCell(withIdentifier: "SaturationDataTableviewCell") as! SaturationDataTableviewCell
                return cell
            }
        }
        else if tableView == self.tableview_otherpayment {
            if indexPath.row == 0 {
                let cell = self.tableview_otherpayment.dequeueReusableCell(withIdentifier: "OtherPaymentTitleTableviewCell") as! OtherPaymentTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_otherpayment.dequeueReusableCell(withIdentifier: "OtherPaymentDataTableviewCell") as! OtherPaymentDataTableviewCell
                return cell
            }
        }
        else if tableView == self.tableview_overtime {
            if indexPath.row == 0 {
                let cell = self.tableview_overtime.dequeueReusableCell(withIdentifier: "OvertimeTitleTableviewCell") as! OvertimeTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_overtime.dequeueReusableCell(withIdentifier: "OvertimeDataTableviewCell") as! OvertimeDataTableviewCell
                return cell
            }
        }
        else {
            return UITableViewCell()
        }
    }
}
