//
//  FormListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class FormListTableviewCell: UITableViewCell {
    
}

class FormListVC: UIViewController {

    @IBOutlet weak var tableview_formlist: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_formlist.estimatedRowHeight = 162.0
        self.tableview_formlist.rowHeight = UITableView.automaticDimension
    }

}

extension FormListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_formlist.dequeueReusableCell(withIdentifier: "FormListTableviewCell") as! FormListTableviewCell
        return cell
    }
}
