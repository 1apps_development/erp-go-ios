//
//  ManageProductServiceVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class ManageProductTitleTableviewCell: UITableViewCell {
    
}

class ManageProductDataTableviewCell: UITableViewCell {
    
}

class ManageProductServiceVC: UIViewController {
    
    @IBOutlet weak var tableviewProduct_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_manageProduct: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewProduct_height.constant = 50.0 * 11
    }

}

extension ManageProductServiceVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_manageProduct.dequeueReusableCell(withIdentifier: "ManageProductTitleTableviewCell") as! ManageProductTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_manageProduct.dequeueReusableCell(withIdentifier: "ManageProductDataTableviewCell") as! ManageProductDataTableviewCell
            return cell
        }
    }
}
