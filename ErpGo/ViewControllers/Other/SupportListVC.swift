//
//  SupportListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class SupportListTableviewCell: UITableViewCell {
    
}

class SupportListVC: UIViewController {

    @IBOutlet weak var tableview_supportlist: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_supportlist.estimatedRowHeight = 180.0
        self.tableview_supportlist.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension SupportListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_supportlist.dequeueReusableCell(withIdentifier: "SupportListTableviewCell") as! SupportListTableviewCell
        return cell
    }
}
