//
//  LeaveReportVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class LeaveReportTitleTableviewCell: UITableViewCell {
    
}

class LeaveReportDataTableviewCell: UITableViewCell {
    
}

class LeaveReportVC: UIViewController {
    
    @IBOutlet weak var tableviewLeaveReport_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_LeaveReport: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewLeaveReport_height.constant = 50.0 * 11
    }

}

extension LeaveReportVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_LeaveReport.dequeueReusableCell(withIdentifier: "LeaveReportTitleTableviewCell") as! LeaveReportTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_LeaveReport.dequeueReusableCell(withIdentifier: "LeaveReportDataTableviewCell") as! LeaveReportDataTableviewCell
            return cell
        }
    }
}
