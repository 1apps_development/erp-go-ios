//
//  ClientDetailsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 11/04/22.
//

import UIKit

class HistoryTitleTableviewCell: UITableViewCell {
    
}

class HistoryDataTableviewCell: UITableViewCell {
    
}

class ClientDetailsVC: UIViewController {

    @IBOutlet weak var tableviewHistory_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_history: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewHistory_height.constant = 60.0 * 6
    }

}

extension ClientDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_history.dequeueReusableCell(withIdentifier: "HistoryTitleTableviewCell") as! HistoryTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_history.dequeueReusableCell(withIdentifier: "HistoryDataTableviewCell") as! HistoryDataTableviewCell
            return cell
        }
    }
}
