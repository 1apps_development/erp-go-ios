//
//  LabelsListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class LabelTitleTableviewCell: UITableViewCell {
    
}

class LabelDataTableviewCell: UITableViewCell {
    
}

class LabelsListVC: UIViewController {

    @IBOutlet weak var tableviewLabel_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_labellist: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewLabel_height.constant = 50.0 * 11
    }

}

extension LabelsListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_labellist.dequeueReusableCell(withIdentifier: "LabelTitleTableviewCell") as! LabelTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_labellist.dequeueReusableCell(withIdentifier: "LabelDataTableviewCell") as! LabelDataTableviewCell
            return cell
        }
    }
}
