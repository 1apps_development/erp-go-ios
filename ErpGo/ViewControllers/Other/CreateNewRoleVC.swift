//
//  CreateNewRoleVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class CreateRoleTableviewCell: UITableViewCell {
    
    @IBOutlet weak var lblRole: UILabel!
}

class CreateNewRoleVC: UIViewController {

    @IBOutlet weak var tableview_role: UITableView!
    @IBOutlet weak var tableviewRole_height: NSLayoutConstraint!
    
    var roleArr = ["User","Role","Client","Product & Service","Constant unit","Constant tax","Constant category","Company settings","Permission"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewRole_height.constant = 118.0 * CGFloat(self.roleArr.count)
    }
    
}

extension CreateNewRoleVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.roleArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 118.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_role.dequeueReusableCell(withIdentifier: "CreateRoleTableviewCell") as! CreateRoleTableviewCell
        cell.lblRole.text = self.roleArr[indexPath.row]
        return cell
    }
}
