//
//  ProjectListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class ProjectListTableviewCell: UITableViewCell {
    
}

class ProjectListVC: UIViewController {

    @IBOutlet weak var tableview_projectlist: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_projectlist.estimatedRowHeight = 208.0
        self.tableview_projectlist.rowHeight = UITableView.automaticDimension
    }

}

extension ProjectListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_projectlist.dequeueReusableCell(withIdentifier: "ProjectListTableviewCell") as! ProjectListTableviewCell
        return cell
    }
}


