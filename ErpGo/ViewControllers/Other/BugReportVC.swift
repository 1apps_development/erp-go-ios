//
//  BugReportVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class BugReportTableviewCell: UITableViewCell {
    
}

class BugReportVC: UIViewController {

    @IBOutlet weak var tableview_bugreport: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension BugReportVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_bugreport.dequeueReusableCell(withIdentifier: "BugReportTableviewCell") as! BugReportTableviewCell
        return cell
    }
}
