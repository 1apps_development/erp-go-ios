//
//  SubscriptionVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class PaymentHistoryTableviewTitleCell: UITableViewCell {
    
}

class PaymentHistoryTableviewDataCell: UITableViewCell {
    
}

class SubscriptionVC: UIViewController {

    @IBOutlet weak var tableviewPayment_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_paymenthistory: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewPayment_height.constant = 50.0 * 11
    }

}

extension SubscriptionVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_paymenthistory.dequeueReusableCell(withIdentifier: "PaymentHistoryTableviewTitleCell") as! PaymentHistoryTableviewTitleCell
            return cell
        }
        else {
            let cell = self.tableview_paymenthistory.dequeueReusableCell(withIdentifier: "PaymentHistoryTableviewDataCell") as! PaymentHistoryTableviewDataCell
            return cell
        }
    }
}
