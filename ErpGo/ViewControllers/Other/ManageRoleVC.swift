//
//  ManageRoleVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 11/04/22.
//

import UIKit

class ManageRoleTableviewCell: UITableViewCell {
    
}

class ManageRoleVC: UIViewController {

    @IBOutlet weak var tableview_roles: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension ManageRoleVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_roles.dequeueReusableCell(withIdentifier: "ManageRoleTableviewCell") as! ManageRoleTableviewCell
        return cell
    }
}
