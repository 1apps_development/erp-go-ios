//
//  SettingsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class SettingsTableViewCell : UITableViewCell
{
    
    @IBOutlet weak var img_settings: UIImageView!
    @IBOutlet weak var lblSettings: UILabel!
}

class SettingsVC: UIViewController {

    @IBOutlet weak var tableview_settings: UITableView!
    
    var settingsArr = ["Business Settings","System Settings","Company Settings","Payment Settings","Zoom-Meeting","Setting","Slack Settings","Twilio Settings","Recaptcha Settings"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension SettingsVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settingsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_settings.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.lblSettings.text = self.settingsArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanySettingsVC") as! CompanySettingsVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 1 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SystemSettingsVC") as! SystemSettingsVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 2 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanySettingsVC") as! CompanySettingsVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 3 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethodVC") as! PaymentMethodVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 4 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MeetingListVC") as! MeetingListVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 5 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ModuleSettingsVC") as! ModuleSettingsVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 6 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SlackSettingsVC") as! SlackSettingsVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 7 {
            
        }
        else if indexPath.row == 8 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ReCaptchaSettingVC") as! ReCaptchaSettingVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
}
