//
//  ProjectTaskStagesVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class ProjectTaskStagesTableviewCell: UITableViewCell {
    
    @IBOutlet weak var lblStage: UILabel!
}

class ProjectTaskStagesVC: UIViewController {

    @IBOutlet weak var tableview_projectstages: UITableView!
    
    var projectStagesArr = ["Review","To do","Done","Canceled"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension ProjectTaskStagesVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projectStagesArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_projectstages.dequeueReusableCell(withIdentifier: "ProjectTaskStagesTableviewCell") as! ProjectTaskStagesTableviewCell
        cell.lblStage.text = self.projectStagesArr[indexPath.row]
        return cell
    }
}
