//
//  TimeSheetListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class TimesheetTableviewCell: UITableViewCell {
    
}

class TimeSheetListVC: UIViewController {

    @IBOutlet weak var tableviewTimesheet_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_timesheet: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewTimesheet_height.constant = 85.0 * 3
    }

}

extension TimeSheetListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_timesheet.dequeueReusableCell(withIdentifier: "TimesheetTableviewCell") as! TimesheetTableviewCell
        return cell
    }
}
