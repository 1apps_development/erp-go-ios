//
//  EmployeePayslipVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class EmployeePaySlipTitleTableviewCell: UITableViewCell {
    
}

class EmployeePaySlipDataTableviewCell: UITableViewCell {
    
}

class EmployeePayslipVC: UIViewController {

    @IBOutlet weak var tableviewPayslip_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_payslip: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewPayslip_height.constant = 50.0 * 11
    }

}

extension EmployeePayslipVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_payslip.dequeueReusableCell(withIdentifier: "EmployeePaySlipTitleTableviewCell") as! EmployeePaySlipTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_payslip.dequeueReusableCell(withIdentifier: "EmployeePaySlipDataTableviewCell") as! EmployeePaySlipDataTableviewCell
            return cell
        }
    }
}
