//
//  CRMSettingsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class CRMSettingsTableviewCell : UITableViewCell {
    
    @IBOutlet weak var lblSettings: UILabel!
}

class CRMSettingsVC: UIViewController {
    
    @IBOutlet weak var tableview_settings: UITableView!
    var CRMSettingArr = ["Pending","On Hold","New","Loss"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension CRMSettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CRMSettingArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_settings.dequeueReusableCell(withIdentifier: "CRMSettingsTableviewCell") as! CRMSettingsTableviewCell
        cell.lblSettings.text = self.CRMSettingArr[indexPath.row]
        return cell
    }
}
