//
//  ManageClientsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class ManageClientsTableviewCell: UITableViewCell {
    
}

class ManageClientsVC: UIViewController {

    @IBOutlet weak var tableview_manageclients: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension ManageClientsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 148.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_manageclients.dequeueReusableCell(withIdentifier: "ManageClientsTableviewCell") as! ManageClientsTableviewCell
        return cell
    }
}
