//
//  ModuleSettingsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class ModuleTableviewCell: UITableViewCell {
    
    @IBOutlet weak var lblModule: UILabel!
}

class ModuleSettingsVC: UIViewController {

    @IBOutlet weak var tableviewModule_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_modulelist: UITableView!
    
    var moduleArr = ["Deal create","Task move create","Task Comment","Support create","Event create","Bill create","Lead To Deal convert","Contact create","Monthly Payslip Generate","Award create","Meeting create","Company Policy create","Budget create","Project create","Task create","Announcement create","Holiday create","Invoice create","Revenue create","Invoice Payment create"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewModule_height.constant = 60.0 * CGFloat(self.moduleArr.count)
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ModuleSettingsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.moduleArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_modulelist.dequeueReusableCell(withIdentifier: "ModuleTableviewCell") as! ModuleTableviewCell
        cell.lblModule.text = self.moduleArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "DealConvertVC") as! DealConvertVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if indexPath.row == 1 {
            
        }
    }
}
