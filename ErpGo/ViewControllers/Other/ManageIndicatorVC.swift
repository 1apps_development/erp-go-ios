//
//  ManageIndicatorVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 07/04/22.
//

import UIKit

class ManageIndicatorTitleTableviewCell: UITableViewCell {
    
}

class ManageIndicatorDataTableviewCell: UITableViewCell {
    
}

class ManageIndicatorVC: UIViewController {
    
    @IBOutlet weak var tableviewIndicator_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_manageIndicator: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewIndicator_height.constant = 60.0 * 11
    }

}

extension ManageIndicatorVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_manageIndicator.dequeueReusableCell(withIdentifier: "ManageIndicatorTitleTableviewCell") as! ManageIndicatorTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_manageIndicator.dequeueReusableCell(withIdentifier: "ManageIndicatorDataTableviewCell") as! ManageIndicatorDataTableviewCell
            return cell
        }
    }
}
