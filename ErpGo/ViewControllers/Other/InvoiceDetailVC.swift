//
//  InvoiceDetailVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 11/04/22.
//

import UIKit

class OrderSummaryTableviewCell: UITableViewCell {
    
}

class CreditNoteTitleTableviewCell: UITableViewCell {
    
}

class CreditNoteDataTableviewCell: UITableViewCell {
    
}


class InvoiceDetailVC: UIViewController {

    @IBOutlet weak var tableviewOrderSummary_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_orderSummary: UITableView!
    @IBOutlet weak var tableviewCreditNote_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_creditNote: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewOrderSummary_height.constant = 70.0 * 3
        self.tableviewCreditNote_height.constant = 60.0 * 5
    }
    
}

extension InvoiceDetailVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableview_orderSummary {
            return 3
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableview_orderSummary {
            return 70.0
        }
        else {
            return 60.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableview_orderSummary {
            let cell = self.tableview_orderSummary.dequeueReusableCell(withIdentifier: "OrderSummaryTableviewCell") as! OrderSummaryTableviewCell
            return cell
        }
        else {
            if indexPath.row == 0 {
                let cell = self.tableview_creditNote.dequeueReusableCell(withIdentifier: "CreditNoteTitleTableviewCell") as! CreditNoteTitleTableviewCell
                return cell
            }
            else {
                let cell = self.tableview_creditNote.dequeueReusableCell(withIdentifier: "CreditNoteDataTableviewCell") as! CreditNoteDataTableviewCell
                return cell
            }
        }
    }
}
