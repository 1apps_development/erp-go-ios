//
//  SystemSettingsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 06/04/22.
//

import UIKit

class SystemSettingsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
