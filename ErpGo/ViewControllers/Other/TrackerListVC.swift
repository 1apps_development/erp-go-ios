//
//  TrackerListVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 04/04/22.
//

import UIKit

class TrackerListTableviewCell: UITableViewCell {
    
}

class TrackerListVC: UIViewController {

    @IBOutlet weak var tableview_Trackerlist: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension TrackerListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_Trackerlist.dequeueReusableCell(withIdentifier: "TrackerListTableviewCell") as! TrackerListTableviewCell
        return cell
    }
}
