//
//  ChatVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 05/04/22.
//

import UIKit

class ChatTableviewCell: UITableViewCell {
    
}

class ChatCollectionviewCell: UICollectionViewCell {
    
}

class ChatVC: UIViewController {

    @IBOutlet weak var tableview_chat: UITableView!
    @IBOutlet weak var collectionview_chat: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview_chat.estimatedRowHeight = 76.0
        self.tableview_chat.rowHeight = UITableView.automaticDimension
    }

    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ChatVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_chat.dequeueReusableCell(withIdentifier: "ChatTableviewCell") as! ChatTableviewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}

extension ChatVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionview_chat.dequeueReusableCell(withReuseIdentifier: "ChatCollectionviewCell", for: indexPath) as! ChatCollectionviewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50.0, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}
