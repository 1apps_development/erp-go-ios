//
//  JobsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 12/04/22.
//

import UIKit

class JobApplicationTableviewCell: UITableViewCell {
    
}

class JobsVC: UIViewController {

    @IBOutlet weak var tableviewJobs_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_jobs: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewJobs_height.constant = 155.0 * 5
    }

}

extension JobsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_jobs.dequeueReusableCell(withIdentifier: "JobApplicationTableviewCell") as! JobApplicationTableviewCell
        return cell
    }
}
