//
//  ManageJobsVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class ManageJobTitleTableviewCell: UITableViewCell {
    
}

class ManageJobDataTableviewCell: UITableViewCell {
    
}

class ManageJobsVC: UIViewController {
    
    @IBOutlet weak var tableviewJobs_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_manageJobs: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewJobs_height.constant = 50.0 * 11
    }

}

extension ManageJobsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tableview_manageJobs.dequeueReusableCell(withIdentifier: "ManageJobTitleTableviewCell") as! ManageJobTitleTableviewCell
            return cell
        }
        else {
            let cell = self.tableview_manageJobs.dequeueReusableCell(withIdentifier: "ManageJobDataTableviewCell") as! ManageJobDataTableviewCell
            return cell
        }
    }
}
