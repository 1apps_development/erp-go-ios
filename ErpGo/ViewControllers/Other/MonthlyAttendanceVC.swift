//
//  MonthlyAttendanceVC.swift
//  ErpGo
//
//  Created by Nikhil Roy on 08/04/22.
//

import UIKit

class MonthlyAttendanceTableviewCell: UITableViewCell {
    
}

class MonthlyAttendanceVC: UIViewController {

    @IBOutlet weak var tableviewMonthlyAttendance_height: NSLayoutConstraint!
    @IBOutlet weak var tableview_MonthlyAttendance: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableviewMonthlyAttendance_height.constant = 85.0 * 3
    }

}

extension MonthlyAttendanceVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview_MonthlyAttendance.dequeueReusableCell(withIdentifier: "MonthlyAttendanceTableviewCell") as! MonthlyAttendanceTableviewCell
        return cell
    }
}
