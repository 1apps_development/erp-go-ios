//
//  ProductsVC.swift
//  ErpGo
//
//  Created by DREAMWORLD on 31/03/22.
//

import UIKit
class AllproductsCell : UITableViewCell
{
    
}
class AttachmentsCell : UITableViewCell
{
    
}
class ProductsVC: UIViewController {

    @IBOutlet weak var Height_ViewAllproducts: NSLayoutConstraint!
    @IBOutlet weak var Tableview_allproductsList: UITableView!
    
    
    @IBOutlet weak var Height_AttachmentsTableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_AttachmentsList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_allproductsList.delegate = self
        self.Tableview_allproductsList.dataSource = self
        self.Tableview_allproductsList.reloadData()
        self.Height_ViewAllproducts.constant = (15 * 50) + 40
        
        
        self.Tableview_AttachmentsList.delegate = self
        self.Tableview_AttachmentsList.dataSource = self
        self.Tableview_AttachmentsList.reloadData()
        self.Height_AttachmentsTableview.constant = 15 * 50
       
    }
    
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Product"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }

}
//MARK: Tableview Methods
extension ProductsVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.Tableview_allproductsList
        {
            return 15
        }
        else  if tableView == self.Tableview_AttachmentsList
        {
            return 15
        }
        else
        {
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.Tableview_allproductsList
        {
            return 50
        }
        else  if tableView == self.Tableview_AttachmentsList
        {
            return 50
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.Tableview_allproductsList
        {
            let cell = self.Tableview_allproductsList.dequeueReusableCell(withIdentifier: "AllproductsCell") as! AllproductsCell
            return cell
        }
        else
        {
            let cell = self.Tableview_AttachmentsList.dequeueReusableCell(withIdentifier: "AttachmentsCell") as! AttachmentsCell
            return cell
        }
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let EditAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        EditAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_edit"),
            title: "Change")
        EditAction.backgroundColor = UIColor.init(named: "App_Bg_Color")
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction,EditAction])
    }
    
    
}
