//
//  DashboardVC.swift
//  ErpGo
//
//  Created by DREAMWORLD on 31/03/22.
//

import UIKit

class StatisticsListCell : UITableViewCell
{
    
    @IBOutlet weak var cell_view: UIView!
}

class AllProductsCell : UITableViewCell
{
    
}

class LatestexcomeCell : UITableViewCell
{
    
}

class InvoicesCell : UITableViewCell
{
    
}

class DashboardVC: UIViewController {
    
    @IBOutlet weak var Tableview_StatisticsList: UITableView!
    @IBOutlet weak var height_TableviewstatisticsList: NSLayoutConstraint!
    
    @IBOutlet weak var Tableview_AllproductsList: UITableView!
    @IBOutlet weak var view_AllproductsHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var Tableview_LatestexcomeList: UITableView!
    @IBOutlet weak var view_LatestexcomeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var Tableview_invoicesList: UITableView!
    @IBOutlet weak var Height_TableviewinvoiceList: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Tableview_StatisticsList.delegate = self
        self.Tableview_StatisticsList.dataSource = self
        self.Tableview_StatisticsList.reloadData()
        self.height_TableviewstatisticsList.constant = 4 * 80
        
        self.Tableview_AllproductsList.delegate = self
        self.Tableview_AllproductsList.dataSource = self
        self.Tableview_AllproductsList.reloadData()
        self.view_AllproductsHeight.constant = (10 * 50) + 40
        
        self.Tableview_LatestexcomeList.delegate = self
        self.Tableview_LatestexcomeList.dataSource = self
        self.Tableview_LatestexcomeList.reloadData()
        self.view_LatestexcomeHeight.constant = (10 * 50) + 40
        
        self.Tableview_invoicesList.delegate = self
        self.Tableview_invoicesList.dataSource = self
        self.Tableview_invoicesList.reloadData()
        self.Height_TableviewinvoiceList.constant = 10 * 140
        
        
    }
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Dashboard"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
}
//MARK: Tableview Methods
extension DashboardVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.Tableview_StatisticsList
        {
            return 4
        }
        else if tableView == self.Tableview_AllproductsList
        {
            return 10
        }
        else if tableView == self.Tableview_LatestexcomeList
        {
            return 10
        }
        else if tableView == self.Tableview_invoicesList
        {
            return 10
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.Tableview_StatisticsList
        {
            return 80
        }
        else if tableView == self.Tableview_AllproductsList
        {
            return 50
        }
        else if tableView == self.Tableview_LatestexcomeList
        {
            return 50
        }
        else if tableView == self.Tableview_invoicesList
        {
            return 140
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.Tableview_StatisticsList
        {
            
            let cell = self.Tableview_StatisticsList.dequeueReusableCell(withIdentifier: "StatisticsListCell") as! StatisticsListCell
            
            setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
            return cell
            
        }
        else if tableView == self.Tableview_AllproductsList {
            
            let cell = self.Tableview_AllproductsList.dequeueReusableCell(withIdentifier: "AllProductsCell") as! AllProductsCell
            return cell
        }
        else if tableView == self.Tableview_invoicesList {
            
            let cell = self.Tableview_invoicesList.dequeueReusableCell(withIdentifier: "InvoicesCell") as! InvoicesCell
            return cell
        }
        else  {
            
            let cell = self.Tableview_LatestexcomeList.dequeueReusableCell(withIdentifier: "LatestexcomeCell") as! LatestexcomeCell
            return cell
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
