//
//  ProjectsVC.swift
//  ErpGo
//
//  Created by DREAMWORLD on 31/03/22.
//

import UIKit

class ProjectStatusCell : UITableViewCell
{
    
}
class TodoListCell : UITableViewCell
{
    
}

class ProjectsVC: UIViewController {

    @IBOutlet weak var Tableview_TodoList: UITableView!
    @IBOutlet weak var Height_todolist: NSLayoutConstraint!
    @IBOutlet weak var Tableview_ProjectstatusList: UITableView!
    @IBOutlet weak var Hight_Tablview_projectstatus: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_ProjectstatusList.delegate = self
        self.Tableview_ProjectstatusList.dataSource = self
        self.Tableview_ProjectstatusList.reloadData()
        self.Hight_Tablview_projectstatus.constant = 4 * 80
        
        self.Tableview_TodoList.delegate = self
        self.Tableview_TodoList.dataSource = self
        self.Tableview_TodoList.reloadData()
        self.Height_todolist.constant = 10 * 100
    }
    
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Project"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }

}
//MARK: Tableview Methods
extension ProjectsVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.Tableview_ProjectstatusList
        {
            return 4
        }
        else if tableView == self.Tableview_TodoList
        {
            return 10
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.Tableview_ProjectstatusList
        {
            return 80
        }
        else if tableView == self.Tableview_TodoList
        {
            return 100
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.Tableview_ProjectstatusList
        {
            
            let cell = self.Tableview_ProjectstatusList.dequeueReusableCell(withIdentifier: "ProjectStatusCell") as! ProjectStatusCell
            
            return cell
            
        }
        else  {
            
            let cell = self.Tableview_TodoList.dequeueReusableCell(withIdentifier: "TodoListCell") as! TodoListCell
            return cell
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
