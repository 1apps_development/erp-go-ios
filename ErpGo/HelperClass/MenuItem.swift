//
//  MenuItem.swift
//  Taskly
//
//  Created by mac on 10/03/22.
//

import Foundation
import UIKit

class MenuItem: Codable{

    var title: String
    var image: String
    var isSelected: Bool
    
    init(title: String, image:String, isselected: Bool) {
        self.title = title
        self.image = image
        self.isSelected = isselected
    }

}

class SettingItem:Codable{
    var title: String
    var image: String
    init(title: String, image:String) {
        self.title = title
        self.image = image
    }
}

class ColorItem: Codable{
    var color: String
    init(colour: String){
        self.color = colour
    }
}

class PermissionItem: Codable{
    var title: String
    var isSelected: Bool
    init( name: String, selected: Bool){
        self.title = name
        self.isSelected = selected
    }
}

class TimeSheetListItem: Codable{
    var day: String
    var date: String
    var time: String
    init(day:String, date: String, time: String){
        self.day = day
        self.date = date
        self.time = time
    }
}

